﻿using System;

namespace Encapsulamiento
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Usuario usuario = new Usuario("jpanchana", "e@a.com");

            usuario.CalcularEdad();

            
            Console.WriteLine(usuario.RetornarDatosUsuario());

        }
    }
}
