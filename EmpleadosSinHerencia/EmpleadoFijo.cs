﻿using System;
namespace EmpleadosSinHerencia
{
    public class EmpleadoFijo : Empleado
    {
       
        public DateTime FechaEntrada { get; set; }

        private decimal SueldoBase { get; set; }
        private decimal Complemento { get; set; }

        public EmpleadoFijo()
        {
            this.SueldoBase = 400;
            this.Complemento = 2;
        }

        private int CalculoAniosEmpresa()
        {
            TimeSpan aniosEmpresa = DateTime.Today - this.FechaEntrada;
            DateTime tiempoTotal = new DateTime(aniosEmpresa.Ticks);
            return tiempoTotal.Year;

        }

        public decimal CalculoSueldo()
        {
            return this.SueldoBase + (this.Complemento * CalculoAniosEmpresa());
        }
    }
}
