﻿using System;
namespace EmpleadosSinHerencia
{
    public class Empleado
    {
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Departamento { get; set; }

        public string NumeroCedula { get; set; }

        //Definir un método CalcularSueldo()
        //

        public Empleado()
        {
        }
    }
}
