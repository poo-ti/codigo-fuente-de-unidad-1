﻿using System;
namespace EmpleadosSinHerencia
{
    public class EmpleadoPorHoras : Empleado
    {
        private decimal PrecioHora { get; set; }//Valor Fijo
        public int NumeroHoras { get; set; }//Variable

        public EmpleadoPorHoras()
        {
            this.PrecioHora = 6;
        }

        public decimal CalcularSueldo()
        {
            return this.PrecioHora * this.NumeroHoras;
        }
    }
}
