﻿using System;
using System.Collections.Generic;

namespace ClasesObjetos
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //Aplicaciones compiladas
            //Una instancia de la clase Usuario
            //Objeto de tipo usuario

            Usuario usuario = new Usuario();
            usuario.username = "jpanchana";
            usuario.email = "jpanchana@uoc.edu";
            usuario.password = "AAAAA";
            //Crear un objeto de tipo DateTime
            DateTime fechaNacimiento = new DateTime(1984, 6, 7);
            usuario.borndate = fechaNacimiento;

            List<string> ListaCompras = new List<string>();
            ListaCompras.Add("lechugas");
            ListaCompras.Add("tomates");
            ListaCompras.Add("pimietos");

            List<int> ListaNumeros = new List<int>();
            ListaNumeros.Add(1);
            ListaNumeros.Add(4);
            ListaNumeros.Add(11);
            ListaNumeros.Add(2);

            //Lista de objetos
            List<Verdura> ListaVerduras = new List<Verdura>();

            Verdura verduraTomate = new Verdura();
            verduraTomate.Nombre = "Tomate";

            ListaVerduras.Add(verduraTomate);


            //objeto usuario

            //C# Java Case sensitive
        }
    }
}

