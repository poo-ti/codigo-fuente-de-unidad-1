﻿using System;
namespace ClasesObjetos
{
    public class Verdura
    {
        public string Nombre { get; set; }
        public string Color { get; set; }
        public decimal Peso { get; set; }
        //...

        public Verdura()
        {
        }
      
    }
}
